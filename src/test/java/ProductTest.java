import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ProductTest {

	Product p;
	
	@BeforeEach
	void CreateNewProduct() {
		p = new Product("sku", 5, "description");
	}
	
	@Test
	void testNegativePrice() {
		assertThrows(IllegalArgumentException.class, () -> {
			new Product("sku", -1, "description");
		});
	}
	
	@Test
	void testGetUnitPrice() {
		assertEquals(p.getUnitPrice(), 5);
	}
	
	@Test
	void testSetUnitPrice() {
		p.setUnitPrice(10);
		assertEquals(p.getUnitPrice(), 10);
	}
	
	@Test
	void testSetUnitPriceNegative() {
		Exception e = assertThrows(IllegalArgumentException.class, () -> {
			p.setUnitPrice(-1);
		});
		assertEquals(e.getMessage(), "Unit price must be non-negative.");
	}
	
	@Test
	void testGetDescription() {
		assertEquals(p.getDescription(), "description");
	}
	
	@Test
	void testSetDescription() {
		p.setDescription("new description");
		assertEquals(p.getDescription(), "new description");
	}
	
	@Test
	void testGetSku() {
		assertEquals(p.getSku(), "sku");
	}
	
	@Test
	void testToString() {
		String sku = p.getSku();
		double price = p.getUnitPrice();
		String description = p.getDescription();
		assertEquals(p.toString(), "Product [sku=" + sku + ", unitPrice=" + price + ", description=" + description + "]");
	}
	
	@Test
	void testEqualsSelf() {
		assertTrue(p.equals(p));
	}
	
	@Test
	void testEqualsNull() {
		assertFalse(p.equals(null));
	}
	
	@Test
	void testEqualsDifferentClass() {
		assertFalse(p.equals(new Customer("A", "Customer")));
	}

	@Test
	void testEqualsDifferentProduct() {
		assertFalse(p.equals(new Product("sku2", 5, "different")));
	}
	
	@Test
	void testEqualsDifferentProductSameSku() {
		assertTrue(p.equals(new Product("sku", 5, "same")));
	}
	
	@Test
	void testEqualsSingleNullSku() {
		Product p2 = new Product(null, 5, "description");
		assertFalse(p2.equals(p));
	}
	
	@Test
	void testEqualsBothNullSku() {
		Product p1 = new Product(null, 5, "description");
		Product p2 = new Product(null, 5, "description");
		assertTrue(p1.equals(p2));
	}
	
}
