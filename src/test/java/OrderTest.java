import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class OrderTest {

	Customer c = new Customer("First", "Last");
	Order o = new Order(c);
	Product p = new Product("sku", 5, "description");
	
	@BeforeEach
	void CreateNewOrder() {
		o = new Order(c);
	}
	
	@Test
	void testNumberIncrements() {
		int number1 = o.getNumber();
		Order order2 = new Order(c);
		assertEquals(order2.getNumber(), number1 + 1);
	}
	
	@Test
	void testGetCustomer() {
		assertEquals(o.getCustomer(), c);
	}
	
	@Test
	void testAddToOrder() {
		o.addToOrder(p, 1);
		assertTrue(o.contains(p));
	}
	
	@Test
	void testAddToOrderExistingProduct() {
		o.addToOrder(p, 1);
		o.addToOrder(p, 1); // order now already contains p
		assertEquals(o.getItemQuantity(p), 2);
	}
	
	@Test
	void testAddToOrderNegativeQuantity() {
		Exception e = assertThrows(IllegalArgumentException.class, () -> {
			o.addToOrder(p, -1);
		});
		assertEquals(e.getMessage(), "Quantity must be non-negative.");
	}
	
	@Test
	void testRemoveFromOrder() {
		o.addToOrder(p, 1);
		o.removeFromOrder(p, 1);
		assertFalse(o.contains(p));
	}
	
	@Test
	void testRemoveFromOrderNegativeQuantity() {
		Exception e = assertThrows(IllegalArgumentException.class, () -> {
			o.removeFromOrder(p, -1);
		});
		assertEquals(e.getMessage(), "Quantity must be non-negative.");
	}
	
	@Test
	void testRemoveFromOrderMissingProduct() {
		assertFalse(o.removeFromOrder(p, 1));
	}
	
	@Test
	void testRemoveFromOrderLessThanQuantity() {
		o.addToOrder(p, 2);
		o.removeFromOrder(p, 1);
		assertEquals(o.getItemQuantity(p), 1);
	}
	
	@Test
	void testGetItemQuantityZero() {
		assertEquals(o.getItemQuantity(p), 0);
	}
	
	@Test
	void testGetOrderTotal() {
		o.addToOrder(p,  3);
		assertEquals(o.getOrderTotal(), 15);
	}
	
	@Test
	void testGetOrderLineCount() {
		o.addToOrder(p,  1);
		assertEquals(o.getOrderLineCount(), 1);
	}
	
	@Test
	void testGetOrderItemCount() {
		o.addToOrder(p,  3);
		assertEquals(o.getOrderItemCount(), 3);
	}
	
	@Test
	void testToString() {
		int number = o.getNumber();
		Customer customer = c;
		HashMap lines = new HashMap<Product, Integer>();
		assertEquals(o.toString(), "Order [number=" + number + ", customer=" + customer + ", lines=" + lines + "]");
	}
	
	@Test
	void testEqualsSelf() {
		assertTrue(o.equals(o));
	}
	
	@Test
	void testEqualsNull() {
		assertFalse(o.equals(null));
	}
	
	@Test
	void testEqualsDifferentClass() {
		assertFalse(o.equals(c));
	}

	@Test
	void testEqualsDifferentOrder() {
		assertFalse(o.equals(new Order(c)));
	}
	
}
