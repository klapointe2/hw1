import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertFalse;


public class CustomerTest {
	
	Customer c;
	
	@BeforeEach
	void CreateNewCustomer() {
		c = new Customer("First", "Last");
	}
	
	@Test
	void testGetFirstName() {
		assertEquals(c.getFirstName(), "First");
	}
	
	@Test
	void testGetLastName() {
		assertEquals(c.getLastName(), "Last");
	}
	
	@Test
	void testSetFirstName() {
		c.setFirstName("Test");
		assertEquals(c.getFirstName(), "Test");
	}
	
	@Test
	void testSetLastName() {
		c.setLastName("Test");
		assertEquals(c.getLastName(), "Test");
	}
	
	@Test
	void testCustomerNumberIncrements() {
		int first = c.getNumber();
		c = new Customer("First", "Last");
		assertEquals(c.getNumber(), first + 1);
	}
	
	@Test
	void testToString() {
		int number = c.getNumber();
		String first = c.getFirstName();
		String last = c.getLastName();
		assertEquals(c.toString(), "Customer [number=" + number + ", name=" + first + " " + last + "]");
	}
	
	@Test
	void testEqualsSelf() {
		assertTrue(c.equals(c));
	}
	
	@Test
	void testEqualsNull() {
		assertFalse(c.equals(null));
	}
	
	@Test
	void testEqualsDifferentClass() {
		assertFalse(c.equals(new Order(c)));
	}

	@Test
	void testEqualsDifferentCustomer() {
		assertFalse(c.equals(new Customer("Different", "Person")));
	}
	
}
